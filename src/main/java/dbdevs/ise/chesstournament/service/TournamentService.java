package dbdevs.ise.chesstournament.service;

import dbdevs.ise.chesstournament.model.Tournament;

import java.util.List;

public interface TournamentService {
    Tournament createTournament(Tournament tournament);
    Tournament getTournament(int id);
    List<Tournament> getTournaments();
}

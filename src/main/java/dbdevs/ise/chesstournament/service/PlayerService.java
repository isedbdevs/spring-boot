package dbdevs.ise.chesstournament.service;

import dbdevs.ise.chesstournament.model.Player;

import java.util.List;

public interface PlayerService {

    Player createPlayer(Player player);

    List<Player> getPlayers();

    Player getPlayer(int id);

    Player updatePlayer(Player player);

    void deletePlayer(Player player);
}

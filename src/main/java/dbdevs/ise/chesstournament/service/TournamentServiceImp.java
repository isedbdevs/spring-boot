package dbdevs.ise.chesstournament.service;

import dbdevs.ise.chesstournament.model.Tournament;
import dbdevs.ise.chesstournament.repository.TournamentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TournamentServiceImp implements TournamentService {
    private final TournamentRepository tournamentRepository;

    public TournamentServiceImp(TournamentRepository tournamentRepository) {
        this.tournamentRepository = tournamentRepository;
    }

    @Override
    public Tournament createTournament(Tournament tournament) {
        return tournamentRepository.save(tournament);
    }

    @Override
    public Tournament getTournament(int id) { return tournamentRepository.getOne(id); }

    @Override
    public List<Tournament> getTournaments(){ return tournamentRepository.findAll(); }
}

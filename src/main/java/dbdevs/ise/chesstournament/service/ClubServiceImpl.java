package dbdevs.ise.chesstournament.service;

import dbdevs.ise.chesstournament.model.Club;
import dbdevs.ise.chesstournament.repository.ClubRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClubServiceImpl implements ClubService {

    private final ClubRepository clubRepository;

    public ClubServiceImpl(ClubRepository clubRepository) {
        this.clubRepository = clubRepository;
    }

    @Override
    public List<Club> getClubs() {
        return clubRepository.findAll();
    }

    @Override
    public Club getClub(int id) {
        return clubRepository.getOne(id);
    }

    @Override
    public Club updateClub(Club club) {
        return clubRepository.save(club);
    }

    @Override
    public void deleteClub(Club club) {
        clubRepository.delete(club);
    }

    @Override
    public Club createClub(Club club) { return clubRepository.save(club);}
}

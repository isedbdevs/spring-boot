package dbdevs.ise.chesstournament.service;

import dbdevs.ise.chesstournament.model.Club;

import java.util.List;

public interface ClubService {

    Club createClub(Club club);

    List<Club> getClubs();

    Club getClub(int id);

    Club updateClub(Club club);

    void deleteClub(Club club);
}

package dbdevs.ise.chesstournament.repository;

import dbdevs.ise.chesstournament.model.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TournamentRepository extends JpaRepository<Tournament, Integer> {

}

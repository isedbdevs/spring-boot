package dbdevs.ise.chesstournament.repository;

import dbdevs.ise.chesstournament.model.Club;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClubRepository extends JpaRepository<Club, Integer> {

}

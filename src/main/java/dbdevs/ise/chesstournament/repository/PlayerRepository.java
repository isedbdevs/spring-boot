package dbdevs.ise.chesstournament.repository;

import dbdevs.ise.chesstournament.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Integer> {
}

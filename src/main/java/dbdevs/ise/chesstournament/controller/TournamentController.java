package dbdevs.ise.chesstournament.controller;

import dbdevs.ise.chesstournament.model.Tournament;
import dbdevs.ise.chesstournament.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tournament")
public class TournamentController {

    private final TournamentService tournamentService;

    @Autowired
    public TournamentController(TournamentService tournamentService) {
        this.tournamentService = tournamentService;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity createTournament(@RequestBody Tournament tournament) {
        try {
            return new ResponseEntity<>(tournamentService.createTournament(tournament), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity getTournaments() {
        try {
            return new ResponseEntity<>(tournamentService.getTournaments(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getTournament(@PathVariable int id) {
        try {
            return new ResponseEntity<>(tournamentService.getTournament(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}

package dbdevs.ise.chesstournament.controller;

import dbdevs.ise.chesstournament.model.Club;
import dbdevs.ise.chesstournament.model.Player;
import dbdevs.ise.chesstournament.service.ClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/club")
public class ClubController {

    private final ClubService clubService;

    @Autowired
    public ClubController(ClubService clubService) {
        this.clubService = clubService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity getClubs() {
        try {
            return new ResponseEntity<>(clubService.getClubs(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getClub(@PathVariable int id) {
        try {
            return new ResponseEntity<>(clubService.getClub(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ResponseEntity updateClub(@PathVariable int id, @RequestBody Club club) {
        try {
            return new ResponseEntity<>(clubService.updateClub(club), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteClub(@PathVariable int id) {
        Club club = clubService.getClub(id);
        try {
            clubService.deleteClub(club);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity createClub(@RequestBody Club club) {
        try {
            return new ResponseEntity<>(clubService.createClub(club), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}

package dbdevs.ise.chesstournament.model;

import javax.persistence.*;

@Entity
@Table(name = "PERSON")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERSON_ID")
    private Integer personId;

    @Column(name = "CLUB_ID")
    private Integer clubId;

    @Column(name = "FIRSTNAME")
    private String firstname;

    @Column(name = "PREFIX")
    private String prefix;

    @Column(name = "LASTNAME")
    private String lastname;

    @Column(name = "GENDER")
    private String gender;

    @Column(name = "STREET_ADDRESS")
    private String address;

    @Column(name = "PHONENUMBER")
    private String phonenumber;

    @Column(name = "ZIPCODE")
    private String zipcode;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ELO")
    private Integer elo;

    @Column(name = "PLACE")
    private String place;

    public Player() {

    }


    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPerson_ID(Integer person_ID) {
        personId = person_ID;
    }

    public Integer getClubId() {
        return clubId;
    }

    public void setClubId(Integer clubId) {
        this.clubId = clubId;
    }

    public Integer getElo() {
        return elo;
    }

    public void setElo(Integer elo) {
        this.elo = elo;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public String toString() {
        return "Player:{" +
                "personId:" + personId +
                ", clubId:" + clubId + '\'' +
                ", firstname:'" + firstname + '\'' +
                ", prefix:'" + prefix + '\'' +
                ", lastname:'" + lastname + '\'' +
                ", gender:'" + gender + '\'' +
                ", address:'" + address + '\'' +
                ", phonenumber:" + phonenumber +
                ", zipcode:'" + zipcode + '\'' +
                ", email:'" + email + '\'' +
                '}';
    }
}

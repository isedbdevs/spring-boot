package dbdevs.ise.chesstournament.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CLUB")
public class Club {

    @Id
    @Column(name = "CLUB_ID")
    private int clubId;

    @Column(name = "CLUB_NAME")
    private String name;

    @Column(name = "CLUB_STREET_ADDRESS")
    private String clubStreetAddress;

    @Column(name = "CLUB_PLACE")
    private String clubPlace;

    public Club() {

    }

    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Club:{" +
                "clubId:" + clubId +
                ", name:'" + name + '\'' +
                '}';
    }

    public String getClubStreetAddress() {
        return clubStreetAddress;
    }

    public void setClubStreetAddress(String clubStreetAddress) {
        this.clubStreetAddress = clubStreetAddress;
    }

    public String getClubPlace() {
        return clubPlace;
    }

    public void setClubPlace(String clubPlace) {
        this.clubPlace = clubPlace;
    }
}

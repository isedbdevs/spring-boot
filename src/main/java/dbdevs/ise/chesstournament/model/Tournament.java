package dbdevs.ise.chesstournament.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "TOURNAMENT")
public class Tournament {
    @Id
    @Column(name = "TOURNAMENT_ID")
    private int tournamentId;

    @Id
    @Column(name = "TOURNAMENT_NAME")
    private String tournamentName;

    @Id
    @Column(name = "TOURNAMENT_DATE")
    private Date tournamentDate;

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Date getTournamentDate() {
        return tournamentDate;
    }

    public void setTournamentDate(Date tournamentDate) {
        this.tournamentDate = tournamentDate;
    }


    @Override
    public String toString() {
        return "Tournament:{" +
                "TournamentId:" + tournamentId +
                ", name:" + tournamentName + '\'' +
                ", date:" + tournamentDate + '\'' +
                '}';
    }
}

package dbdevs.ise.chesstournament.service;

import dbdevs.ise.chesstournament.model.Player;
import dbdevs.ise.chesstournament.repository.PlayerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PlayerServiceTest {

    @MockBean
    private PlayerRepository playerRepository;

    private PlayerService playerService;

    private Player player;

    @Before
    public void setup() {
        playerService = new PlayerServiceImpl(playerRepository);
        player = new Player();
        player.setPersonId(1);
        player.setFirstname("R");
        player.setLastname("B");
    }

    @Test
    public void shouldCreatePlayer() {
        when(playerRepository.save(player)).thenReturn(player);

        Player player1 = playerService.createPlayer(player);

        assertThat(player1)
                .isEqualTo(player);
    }

    @Test
    public void shouldGetPlayers() {
        when(playerRepository.findAll()).thenReturn(Collections.singletonList(player));

        List<Player> players = playerService.getPlayers();

        assertThat(players)
                .isEqualTo(Collections.singletonList(player));
    }


    @Test
    public void shouldGetPlayer() {
        when(playerRepository.getOne(1)).thenReturn(player);

        Player player1 = playerService.getPlayer(1);

        assertThat(player1)
                .isEqualTo(player);
    }

    @Test
    public void shouldUpdatePlayer() {
        when(playerRepository.save(player)).thenReturn(player);
        player.setLastname("C");

        Player player1 = playerService.updatePlayer(player);

        assertThat(player1.getLastname())
                .isEqualTo("C");
    }
}

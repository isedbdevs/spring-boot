//package dbdevs.ise.chesstournament.controller;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import dbdevs.ise.chesstournament.model.Player;
//import dbdevs.ise.chesstournament.service.PlayerService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest
//public class PlayerControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private PlayerService service;
//
//    private Player player;
//
//    @Autowired
//    ObjectMapper objectMapper;
//
//    @Before
//    public void setup() {
//        player = new Player();
//        player.setPersonId(1);
//        player.setFirstname("R");
//        player.setLastname("B");
//    }
//
////    @Test
////    public void shouldReturnPlayers() {
////        List<Player> players = singletonList(player);
////
////        when(service.getPlayers()).thenReturn(players);
////
////        try {
////            this.mockMvc.perform(
////                    get("/player/"))
////                    .andExpect(status().isOk());
////        } catch (Exception e) {
////            System.out.println(e.getMessage());
////        }
////    }
//
//    @Test
//    public void shouldCreatePlayer() {
//
//        when(service.createPlayer(player)).thenReturn(player);
//
//        try {
//            this.mockMvc.perform(
//                    post("/player/")
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(objectMapper.writeValueAsString(player)))
//                    .andExpect(status().isCreated());
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }
//
////    @Test
////    public void shouldGetPlayer() {
////        when(service.getPlayer(1)).thenReturn(player);
////
////        try {
////            this.mockMvc.perform(
////                    get("/player/" + player.getPersonId()))
////                    .andExpect(status().isOk());
////        } catch (Exception e) {
////            System.out.println(e.getMessage());
////        }
////    }
//
//    @Test
//    public void ShouldUpdatePlayer() {
//        when(service.updatePlayer(player)).thenReturn(player);
//
//        try {
//            this.mockMvc.perform(
//                    post("/player/" + player.getPersonId())
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(objectMapper.writeValueAsString(player)))
//                    .andExpect(status().isOk());
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }
//
//    @Test
//    public void ShouldDeletePlayer() {
//        try {
//            this.mockMvc.perform(
//                    delete("/player/" + player.getPersonId()))
//                    .andExpect(status().isNoContent());
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }
//}